from guardian import GuardState, GuardStateDecorator
import tcsconfig

nominal = 'DOWN'
request = 'DOWN'

##################################################
# decorators for checking laser status

# we should include a timer here to elimiate glitches.
# If we see a condition of laser fault, repeat for 2s?
class assert_is_laser_on(GuardStateDecorator):
    def pre_exec(self):
        if ezca['LSRPWR_HD_PD_OUTPUT'] < 35 or ezca['PWR_SUPPLY_I_OUTPUT'] < 5:
            return 'LASERFAULT'

class assert_is_coolant_flowing(GuardStateDecorator):
    def pre_exec(self):
        if ezca['FLOWRATE'] < 1:
            notify('Flow rate is reading low.')

class assert_power_is_correct(GuardStateDecorator):
    def pre_exec(self):
        if (ezca['LSRPWR_MTR_OUTPUT'] > tcsconfig.power_meter_output_high[SYSTEM]) or (ezca['LSRPWR_MTR_OUTPUT'] < tcsconfig.power_meter_output_low[SYSTEM]):
            notify('TCS laser power is outside the correct setting range')

##################################################

# define the DOWN state
class DOWN(GuardState):
    index = 10
    request = True
    goto = True

    def main(self):
        return True

    def run(self):
        return True

# define the LASERFAULT state
class LASERFAULT(GuardState):
    index = 30
    redirect = False
    request = False
    
    # use this to diagnose the problem
    # run through this check list
    def run(self):
        # check if the laser is supposed to be on. Only proceed if YES
        if ezca['LASERONOFFSWITCH'] == 'NotValid':
            notify('Warning 501 (E1300233): Laser has been turned off via EPICS. Use EPICS to turn on laser again.')
            return False

        # Check if the interlock flow alarm has triggered. Only proceed if NO
        if ezca['INTRLK_FLOW_ALRM'] == 'Valid':
            if ezca['FLOWRATE'] > 2:
                notify('Fault 1003 (E1300233): Shut down chiller. Coolant flow is low but non-zero. Contact TCS.')
                return False
            else:
                notify('Fault 1004 (E1300233): Flow alarm triggered. No coolant. Check chiller. Cycle laser key.')
                return False

        # Check if the interlock RTD alarm has triggered. Only proceed if NO.
        if ezca['INTRLK_RTD_OR_IR_ALRM'] == 'Valid':
            notify('Fault 1005 (E1300233): RTD/IR alarm has triggered. Reset CO2 laser controller.')
            return False

        # check for failure of half of RF drive
        if ezca['LSRPWR_HD_PD_OUTPUT'] < 35 and ezca['LSRPWR_HD_PD_OUTPUT'] > 20:
            notify('Fault 1006 (E1300233): Possible RF drive problem. Contact TCS. Full CO2 shutdown recommended.')

        #--------------------------------------------------------------------------------------------    
        # check for the existence of the fault again. Only proceed if these are cleared
        if ezca['LSRPWR_HD_PD_OUTPUT'] <= 20:
            notify('Fault 1001 (E1300233): No power from laser. Cycle laser key. Check chiller for faults.')
            return False

        if ezca['PWR_SUPPLY_I_OUTPUT'] < 5:
            notify('Fault 1002 (E1300233): Laser shows power, but supply problem exists. Hardware problem? See manual.')
            return False

        # if this stage is reached, all faults should be cleared. Return to LASER ON
        return 'LASERON'

    
class LASERON(GuardState):
    index = 100

    @assert_is_laser_on
    @assert_power_is_correct
    def main(self):
        pass

    @assert_is_laser_on
    @assert_power_is_correct
    def run(self):
        return True
            
##################################################

edges = [
    ('INIT', 'LASERON'),
    ('INIT', 'DOWN'),
    ('DOWN','LASERON')
    ]
